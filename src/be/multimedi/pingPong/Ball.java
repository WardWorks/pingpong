package be.multimedi.pingPong;

import java.awt.*;

public class Ball extends Sprite {
   static final int width = 8;
   static final int height = 8;

   float posXf;
   float posYf;
   float vX = 60;
   float vY = 24;

   public Ball(float posXf, float posYf, float vX, float vY) {
      this.vX = vX;
      this.vY = vY;
      this.posXf = posXf - width/2;
      this.posYf = posYf - height/2;
      posX = (int)this.posXf;
      posY = (int)this.posYf;
   }

   @Override
   public void draw(Graphics g) {
      g.setColor(Color.WHITE);
      g.drawOval(posX, posY, width,height);
   }

   @Override
   public void update(float timeDelta) {
      posXf += vX*timeDelta;
      posYf += vY*timeDelta;
      posX = (int)posXf;
      posY = (int)posYf;
   }
}
